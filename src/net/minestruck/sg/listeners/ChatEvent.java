package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.BountyUtil;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvent implements Listener{
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event){
		String m = event.getMessage();
		Player p = event.getPlayer();

		if(Core.playing.contains(p)){
			String format = "�7�o[<points>] �9<player>�8: �7<message>";
			format.replace("<player>", p.getDisplayName()); 
			format.replace("<message>", m);
			format.replace("<points>", String.valueOf(BountyUtil.getPoints(p)));
			event.setFormat(format);
			event.setMessage(m);
		}
		else if(Core.spectating.contains(p.getName())){
			String format = "�7�o[<points>] �7<player>�8: �7<message>";
			format.replace("<player>", p.getDisplayName()); 
			format.replace("<message>", m);
			format.replace("<points>", String.valueOf(BountyUtil.getPoints(p)));
			event.setFormat(format);
			event.setMessage(m);
			for(Player pl : Core.playing){
				event.getRecipients().remove(pl);
			}
		}
		else if(Core.resurrected.contains(p)){
			String format = "�7�o[<points>] �d<player>�8: �7<message>";
			format.replace("<player>", p.getDisplayName()); 
			format.replace("<message>", m);
			format.replace("<points>", String.valueOf(BountyUtil.getPoints(p)));
			event.setFormat(format);
			event.setMessage(m);
		}
	}
}
