package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.ChestUtil;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerOpenChest implements Listener{
	@EventHandler
	public void openChest(PlayerInteractEvent event){
		if(event.getClickedBlock() != null){
			Block b = event.getClickedBlock();
			BlockState bs = b.getState();
			if(bs instanceof Chest){
				Chest c = (Chest) bs;
				if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
					if(event.getClickedBlock().getType() == Material.CHEST){
						if(!Core.opened.contains(c.getLocation())){
							if(Core.filed.contains(0)){
								ChestUtil.fillChest(c);
								Core.opened.add(c.getLocation());
							}
						}
					}
				}
			}
		}
		if(Core.spectating.contains(event.getPlayer().getName())){
			event.setCancelled(true);
		}
	}
}
