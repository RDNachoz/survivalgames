package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;
import net.minestruck.sg.util.SpectatorUtil;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerRespawn implements Listener{

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event){
		Player p = event.getPlayer();
		SpectatorUtil.spectate(p);
	}

	@EventHandler
	public void onMove(PlayerMoveEvent event){
		final Player p = event.getPlayer();
		if(GameStateUtil.gs == GameState.PREGAME || GameStateUtil.gs == GameState.PREDM){
			if (event.getFrom().getBlockX() != event.getTo().getBlockX() ||
					event.getFrom().getBlockY() != event.getTo().getBlockY() ||
					event.getFrom().getBlockZ() != event.getTo().getBlockZ()
					){
				Location from=event.getFrom();
				Location to=event.getTo();
				double x=Math.floor(from.getX());
				double z=Math.floor(from.getZ());
				if(Math.floor(to.getX())!=x||Math.floor(to.getZ())!=z){
					x+=.5;
					z+=.5;
					p.teleport(new Location(from.getWorld(),x,from.getY(),z,from.getYaw(),from.getPitch()));
				}
			}
		}
		if(Core.playing.contains(p)){
			final Player target = Core.getNearest(p, 750.0);
			new BukkitRunnable(){
				@Override
				public void run(){
					p.setCompassTarget(target.getLocation());
				}
			}.runTaskTimerAsynchronously(Core.getInstance(), 0L, 5L);
		}
	}
}
