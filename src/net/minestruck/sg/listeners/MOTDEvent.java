package net.minestruck.sg.listeners;

import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class MOTDEvent implements Listener{
	@EventHandler
	public void onPing(ServerListPingEvent event){
		if(GameStateUtil.gs == GameState.STARTED){
			event.setMotd("�a�lJOINABLE");
		}
		else if(GameStateUtil.gs == GameState.VOTING){
			event.setMotd("�d�lVOTING");
		}
		else if(GameStateUtil.gs == GameState.DEATHMATCH || GameStateUtil.gs == GameState.PREGAME ||GameStateUtil.gs == GameState.RESTARTING || GameStateUtil.gs == GameState.PREDM){
			event.setMotd("�c�lNOT JOINABLE");
		}
	}
}
