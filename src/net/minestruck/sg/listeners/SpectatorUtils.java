package net.minestruck.sg.listeners;

import java.util.ArrayList;
import java.util.Map.Entry;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;
import net.minestruck.sg.util.ResUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SpectatorUtils implements Listener{
	ItemStack paper = new ItemStack(Material.PAPER);

	@EventHandler
	public void onDamage(EntityDamageEvent event){
		Player p = null;
		if(event.getEntity() instanceof Player){
			p = (Player) event.getEntity();
		}
		else return;
		if(Core.spectating.contains(p.getName())){
			event.setCancelled(true);
		}
		if(GameStateUtil.gs == GameState.VOTING || GameStateUtil.gs == GameState.PREGAME){
			event.setCancelled(true);
		}
	}


	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event){
		if(event.getDamager() instanceof Projectile){
			if(event.getEntity() instanceof Player){
				Player p = (Player) event.getEntity();
				if(Core.spectating.contains(p.getName())){
					p.kickPlayer("�4Please do not block the arrows.");
					event.setCancelled(true);
				}
				if(GameStateUtil.gs == GameState.VOTING || GameStateUtil.gs == GameState.PREGAME){
					event.setCancelled(true);
				}
			}
		}
		if(event.getDamager() instanceof Player){
			if(event.getEntity() instanceof Player){
				Player p = (Player)event.getDamager();
				if(Core.spectating.contains(p.getName())){
					event.setCancelled(true);
				}
				if(GameStateUtil.gs == GameState.VOTING || GameStateUtil.gs == GameState.PREGAME){
					event.setCancelled(true);

				}
			}
		}
	}

	@EventHandler
	public void onFish(PlayerFishEvent event){
		if(GameStateUtil.gs == GameState.PREDM){
			event.setCancelled(true);
		}
	}
	int i = 0;
	@EventHandler
	public void MobNerf(EntityTargetEvent event){
		Entity target = event.getTarget();
		Entity e = event.getEntity();
		if (e instanceof Player) {
			return;
		}
		if(target instanceof Player){
			String targetName = ((Player) target).getName();
			if(Core.spectating.contains(targetName)){
				event.setTarget(null);
			}
		}
	}
	@EventHandler
	public void onClick(PlayerInteractEvent event){
		Player p = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(Core.spectating.contains(p.getName())){
				if(event.getItem().getType() == Material.ENCHANTED_BOOK){
					openSpecInv(p);
				}
				if(event.getItem().getType() == Material.BONE){
					for (Entry<Player, Player> entry : Core.resurrections.entrySet()){
						Player resurrecting = entry.getKey();
						Player target = entry.getValue();
						ResUtil.resurrect(resurrecting, target);
					}
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInvClick(InventoryClickEvent event){
		Player p = (Player) event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inv = event.getInventory();
		if(clicked == null){
			return;
		}
		if(inv.getName().equals("�e�lTRIBUTES")){
			if(clicked.getType() == Material.PAPER){
				ItemMeta im = clicked.getItemMeta();
				final String name = im.getDisplayName();
				p.teleport(Bukkit.getPlayer(name));
			}
		}
	}

	public void openSpecInv(Player p){
		Inventory inventory = Bukkit.createInventory(null, 27, "�e�lTRIBUTES");
		for(Player pl : Core.playing){
			ItemMeta meta = (ItemMeta) paper.getItemMeta();
			meta.setDisplayName(pl.getName());
			ArrayList<String> lore = new ArrayList<String>();
			lore.add("�3�lCLICK �7to teleport.");
			lore.add(" ");
			for(int i = 0; i <= Core.playing.size() - 1; i++){
				lore.add("�8�l/ / / / �7�lINFORMATION �8�l/ / / /");
				lore.add("�7Health: �b�l" + String.valueOf(Math.round(Core.playing.get(i).getHealth())) + "/ 20");
				lore.add("�7Hunger: �b�l" + String.valueOf(Core.playing.get(i).getFoodLevel() + "/ 20"));
				lore.add("�7Saturation: �b�l" + String.valueOf(Core.playing.get(i).getSaturation()));
				lore.add("�7EXP Levels: �b�l" + String.valueOf(Core.playing.get(i).getLevel()));
				lore.add(" ");
				lore.add("�8�l/ / / / �7�lARMOR �8�l/ / / /");
				if(Core.playing.get(i).getInventory().getHelmet() != null){
					lore.add("�7Helmet: �b�l" + String.valueOf(Core.playing.get(i).getInventory().getHelmet().getType().toString().toLowerCase().split("_")));
				}else{
					lore.add("�7Helmet: �b�lNONE");
				}
				if(Core.playing.get(i).getInventory().getChestplate() != null){
					lore.add("�7Chestplate: �b�l" + String.valueOf(Core.playing.get(i).getInventory().getChestplate().getType().toString().toLowerCase().split("_")));
				}else{
					lore.add("�7Chestplate: �b�lNONE");
				}
				if(Core.playing.get(i).getInventory().getLeggings() != null){
					lore.add("�7Pants: �b�l" + String.valueOf(Core.playing.get(i).getInventory().getLeggings().getType().toString().toLowerCase().split("_")));
				}else{
					lore.add("�7Pants: �b�lNONE");
				}
				if(Core.playing.get(i).getInventory().getBoots() != null){
					lore.add("�7Boots: �b�l" + String.valueOf(Core.playing.get(i).getInventory().getBoots().getType().toString().toLowerCase().split("_")));
				}else{
					lore.add("�7Boots: �b�lNONE");
				}
			}
			meta.setLore(lore);
			paper.setItemMeta(meta);
			inventory.addItem(paper);
		}
		p.openInventory(inventory);
	}
}
