package net.minestruck.sg.listeners;

import java.util.ArrayList;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.BountyUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopEvents implements Listener{

	ItemStack gold_boots = new ItemStack(Material.GOLD_BOOTS, 1);
	ItemStack gold_helm = new ItemStack(Material.GOLD_HELMET, 1);
	ItemStack gold_chest = new ItemStack(Material.GOLD_CHESTPLATE, 1);
	ItemStack gold_pants = new ItemStack(Material.GOLD_LEGGINGS, 1);
	ItemStack wood_sword = new ItemStack(Material.WOOD_SWORD, 1);
	ItemStack stone_axe = new ItemStack(Material.STONE_AXE, 1);
	ItemStack xp = new ItemStack(Material.EXP_BOTTLE, 4);
	ItemStack bow = new ItemStack(Material.BOW, 1);
	ItemStack arrow = new ItemStack(Material.ARROW, 8);
	ItemStack soup = new ItemStack(Material.MUSHROOM_SOUP, 1);
	ItemStack fish = new ItemStack(Material.FISHING_ROD, 1);

	public ArrayList<String> l1 = new ArrayList<String>();
	public ArrayList<String> l2 = new ArrayList<String>();
	public ArrayList<String> l3 = new ArrayList<String>();
	public ArrayList<String> l4 = new ArrayList<String>();
	public ArrayList<String> l5 = new ArrayList<String>();
	public ArrayList<String> l6 = new ArrayList<String>();
	public ArrayList<String> l7 = new ArrayList<String>();
	public ArrayList<String> l8 = new ArrayList<String>();
	public ArrayList<String> l9 = new ArrayList<String>();
	public ArrayList<String> l10 = new ArrayList<String>();
	public ArrayList<String> l11 = new ArrayList<String>();

	@EventHandler
	public void onClick(PlayerInteractEvent event){
		Player p = event.getPlayer();
		Block b = event.getClickedBlock();
		if (b == null) {
			return;
		}
		if(event.getAction()== Action.RIGHT_CLICK_BLOCK){
			if(b.getType()== Material.SIGN || b.getType()==Material.SIGN_POST || b.getType()==Material.WALL_SIGN){
				Sign s = (Sign) b.getState();
				if(s.getLine(0).equalsIgnoreCase("[shop]")){
					if(!Core.spectating.contains(p.getName()) || !Core.resurrected.contains(p)){

						l1.add("�a�nPrice: �b100");
						l2.add("�a�nPrice: �b150");
						l3.add("�a�nPrice: �b150");
						l4.add("�a�nPrice: �b100");
						l5.add("�a�nPrice: �b75");
						l6.add("�a�nPrice: �b75");
						l7.add("�a�nPrice: �b50");
						l8.add("�a�nPrice: �b30");
						l9.add("�a�nPrice: �b200");
						l10.add("�a�nPrice: �b100");
						l11.add("�a�nPrice: �b125");

						gold_helm.getItemMeta().setLore(l1);
						gold_chest.getItemMeta().setLore(l2);
						gold_pants.getItemMeta().setLore(l3);
						gold_boots.getItemMeta().setLore(l4);
						wood_sword.getItemMeta().setLore(l5);
						stone_axe.getItemMeta().setLore(l6);
						fish.getItemMeta().setLore(l7);
						soup.getItemMeta().setLore(l8);
						xp.getItemMeta().setLore(l9);
						bow.getItemMeta().setLore(l10);
						arrow.getItemMeta().setLore(l11);
						
						gold_helm.setItemMeta(gold_helm.getItemMeta());
						gold_chest.setItemMeta(gold_chest.getItemMeta());
						gold_pants.setItemMeta(gold_pants.getItemMeta());
						gold_boots.setItemMeta(gold_boots.getItemMeta());
						wood_sword.setItemMeta(wood_sword.getItemMeta());
						stone_axe.setItemMeta(stone_axe.getItemMeta());
						fish.setItemMeta(fish.getItemMeta());
						soup.setItemMeta(soup.getItemMeta());
						xp.setItemMeta(xp.getItemMeta());
						bow.setItemMeta(bow.getItemMeta());
						arrow.setItemMeta(arrow.getItemMeta());

						openShop(p);
					}
				}else{
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent event){
		Player p = (Player) event.getWhoClicked();
		ItemStack clicked = event.getCurrentItem();
		Inventory inv = event.getInventory();
		if(clicked == null){
			return;
		}
		if(inv.getName().equals("�e�lSHOP")){
			if(clicked.getType() == Material.GOLD_HELMET){
				BountyUtil.removePoints(p, 100);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(gold_helm);
			}
			else if(clicked.getType() == Material.GOLD_CHESTPLATE){
				BountyUtil.removePoints(p, 150);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(gold_chest);
			}
			else if(clicked.getType() == Material.GOLD_LEGGINGS){
				BountyUtil.removePoints(p, 150);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(gold_pants);
			}
			else if(clicked.getType() == Material.GOLD_BOOTS){
				BountyUtil.removePoints(p, 100);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(gold_boots);
			}
			else if(clicked.getType() == Material.WOOD_SWORD){
				BountyUtil.removePoints(p, 75);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(wood_sword);
			}
			else if(clicked.getType() == Material.STONE_AXE){
				BountyUtil.removePoints(p, 75);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(stone_axe);
			}
			else if(clicked.getType() == Material.FISHING_ROD){
				BountyUtil.removePoints(p, 50);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(fish);
			}
			else if(clicked.getType() == Material.MUSHROOM_SOUP){
				BountyUtil.removePoints(p, 30);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(soup);
			}
			else if(clicked.getType() == Material.EXP_BOTTLE){
				BountyUtil.removePoints(p, 200);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(xp);
			}
			else if(clicked.getType() == Material.BOW){
				BountyUtil.removePoints(p, 100);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(bow);
			}
			else if(clicked.getType() == Material.ARROW){
				BountyUtil.removePoints(p, 125);
				p.closeInventory();
				p.sendMessage("�aYou just bought something from the shop!");
				p.getInventory().addItem(arrow);
			}
		}
	}

	public void openShop(Player p) {
		Inventory inv = Bukkit.createInventory(null, 36, "�e�lSHOP");
		inv.setItem(0, gold_helm);
		inv.setItem(9, gold_chest);
		inv.setItem(18, gold_pants);
		inv.setItem(27, gold_boots);
		inv.setItem(10, wood_sword);
		inv.setItem(11, stone_axe);
		inv.setItem(12, fish);
		inv.setItem(35, soup);
		inv.setItem(34, xp);
		inv.setItem(8, bow);
		inv.setItem(17, arrow);
		p.openInventory(inv);
	}
}
