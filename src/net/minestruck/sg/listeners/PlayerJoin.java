package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.BountyUtil;
import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;
import net.minestruck.sg.util.SpectatorUtil;
import net.minestruck.sg.util.VotingUtil;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener{
	Core plugin;
	public PlayerJoin(Core instance){
		plugin = instance;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player p = event.getPlayer();
		p.setAllowFlight(false);
		p.setFlying(false);
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		p.getActivePotionEffects().clear();
		p.teleport(Bukkit.getWorld("spawn").getSpawnLocation());
		VotingUtil.createScoreboard(p);
		if(GameStateUtil.gs == GameState.VOTING){
			Core.playing.add(p);
			for(Player all : Bukkit.getOnlinePlayers()){
				all.setWalkSpeed(0.2F);
			}
		}
		if(!BountyUtil.containsElement(p.getUniqueId().toString())){
			BountyUtil.addPlayer(p);
		}
		else if(GameStateUtil.gs == GameState.OFFLINE || GameStateUtil.gs == GameState.RESTARTING){
			p.kickPlayer("�c�lGAME IS OFFLINE/RESTARTING");
		}
		else if(GameStateUtil.gs == GameState.STARTED){
			Core.spectating.add(p.getName());
			SpectatorUtil.spectate(p);
		}
	}

}
