package net.minestruck.sg.listeners;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockEvent implements Listener{
	List<Material> breakableBlocks = Arrays.asList(new Material[]{
			Material.AIR,
			Material.LEAVES,
			Material.WEB,
			Material.LONG_GRASS,
			Material.DEAD_BUSH,
			Material.YELLOW_FLOWER,
			Material.RED_ROSE,
			Material.BROWN_MUSHROOM,
			Material.RED_MUSHROOM,
			Material.FIRE,
			Material.MELON,
			Material.MELON_BLOCK,
			Material.COCOA
	});
	List<Material> placableBlocks = Arrays.asList(new Material[]{
			Material.AIR,
			Material.LONG_GRASS,
			Material.DEAD_BUSH,
			Material.YELLOW_FLOWER,
			Material.RED_ROSE,
			Material.CAKE_BLOCK,
			Material.CAKE,
			Material.BROWN_MUSHROOM,
			Material.RED_MUSHROOM
	});
	@EventHandler
	public void onBreak(BlockBreakEvent event){
		Block b = event.getBlock();
		Player p = event.getPlayer();
		if(!breakableBlocks.contains(b.getType())) {
			p.sendMessage("�4�l>> �cNO BREAKY BREAKY!");
			event.setCancelled(true);
			return;
		}
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent event){
		Player p = event.getPlayer();
		Block b = event.getBlock();
		if(!placableBlocks.contains(b.getType())) {
			p.sendMessage("�4�l>> �cNO PLACEY PLACEY!");
			event.setCancelled(true);
			return;
		}
	}

}
