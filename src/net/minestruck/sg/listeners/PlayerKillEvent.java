package net.minestruck.sg.listeners;

import java.util.Map.Entry;
import java.util.Random;

import me.libraryaddict.disguise.DisguiseAPI;
import net.minestruck.sg.Core;
import net.minestruck.sg.util.BountyUtil;
import net.minestruck.sg.util.GameUtil;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerKillEvent implements Listener{
	Core plugin;
	public PlayerKillEvent(Core instance){
		plugin = instance;
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDeath(PlayerDeathEvent event){
		final Player killed = event.getEntity();
		final Player killer = event.getEntity().getKiller();
		String message = event.getDeathMessage();

		Core.playing.remove(killed);
		Core.spectating.add(killed.getName());
		String[] customMessage = new String[] {
				"�c�l>> �9"+killed.getName()+" �9forgot their parachute!",
				"�c�l>> �9"+killed.getName()+" �9dropped their phone in a hole and tried to get it!",
				"�c�l>> �9"+killed.getName()+" �9didn't turn on no-fall in time!",
				"�c�l>> �9"+killed.getName()+" �9realized he's not a robot!",
				"�c�l>> �9"+killed.getName()+" �9missed the bridge!",
				"�c�l>> �9"+killed.getName()+" �9fell in a well!"
		};
		if(killer != null){
			for(Player ap : Bukkit.getOnlinePlayers()){
				ap.playSound(ap.getLocation(), Sound.EXPLODE, 10, 10);
			}
			Random chance = new Random();
			if (message.contains("fell")) {
				event.setDeathMessage(customMessage[chance.nextInt(customMessage.length)]);
				int ramount = BountyUtil.getPoints(killed) / 10;
				int rremainder = ramount % 10;
				int rf = ramount + Math.round(rremainder) + 2;
				BountyUtil.removePoints(killed, rf);
			}
			else if(message.contains("slain by")){
				event.setDeathMessage("�c�l>> �9" + killer.getName() + " �9has killed �9" + killed.getName() + "!");
				if(Core.resurrections.containsKey(killer)){
					event.setDeathMessage("�c�l>> �a" + killer.getName() + " �9has taken revenge on �a" + killed.getName() + "�9!");
					DisguiseAPI.undisguiseToAll(killer);
					Core.resurrections.remove(killer, killed);
					Core.playing.add(killer);
					killer.getInventory().clear();
					killer.getInventory().setArmorContents(null);

					int amount = BountyUtil.getPoints(killer) / 10;
					int remainder = amount % 10;
					int f = amount + Math.round(remainder) + 2;
					BountyUtil.addPoints(killer, f);

					int ramount = BountyUtil.getPoints(killed) / 10;
					int rremainder = ramount % 10;
					int rf = ramount + Math.round(rremainder) + 2;
					BountyUtil.removePoints(killed, rf);
				}

				Core.resurrections.put(killed, killer);

				int amount = BountyUtil.getPoints(killer) / 10;
				int remainder = amount % 10;
				int f = amount + Math.round(remainder) + 2;
				BountyUtil.addPoints(killer, f);

				int ramount = BountyUtil.getPoints(killed) / 10;
				int rremainder = ramount % 10;
				int rf = ramount + Math.round(rremainder) + 2;
				BountyUtil.removePoints(killed, rf);
			}
			else if(message.contains("shot by")){
				double distance = killer.getLocation().distance(killed.getLocation());
				double fDistance = Math.round(distance);
				event.setDeathMessage("�c�l>> �9" + killer.getName() + " �9has shot �9" + killed.getName() + " �9from " 
						+ fDistance + " blocks away!");
				Core.resurrections.put(killed, killer);
				int amount = BountyUtil.getPoints(killer) / 10;
				int remainder = amount % 10;
				int f = amount + Math.round(remainder) + 2;
				BountyUtil.addPoints(killer, f);
				int ramount = BountyUtil.getPoints(killed) / 10;
				int rremainder = ramount % 10;
				int rf = ramount + Math.round(rremainder) + 2;
				BountyUtil.removePoints(killed, rf);
				Core.resurrections.put(killed, killer);
				if(BountyUtil.bounties.containsKey(killer)){
					for(Entry<Player, Integer> entry : BountyUtil.bounties.entrySet()){
						Player pl = entry.getKey();
						int points = entry.getValue();
						if(pl == killed){
							BountyUtil.addPoints(killer, points);
						}
					}
				}
			}
			killer.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 10, 0));
			killer.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 5, 0));
		}
		new BukkitRunnable(){
			@Override
			public void run(){
				if(killer != null){
					killed.sendMessage("�a�l>> �4" + killer.getName() + " �4had �c" + killer.getHealth() + " �4when you died!");
					killed.sendMessage("�a�l>> �aIf you have passes you can resurrect to kill �c" + killer.getName());
					killed.sendMessage("�a�l>> �aTo go back to the hub click the �e�lBED");
				}else{
					killed.sendMessage("�a�l>> �aTo go back to the hub click the �e�lBED");
				}
			}
		}.runTaskLater(plugin, 60);

		if(Core.playing.size() == 1){
			GameUtil.win(Bukkit.getOfflinePlayer(killer.getName()));
		}
		if(Core.playing.size() == 0){
			GameUtil.win(Bukkit.getOfflinePlayer("No One"));
		}
	}
}
