package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ResListener implements Listener{
	@EventHandler
	public void onDrop(PlayerDropItemEvent event){
		if(Core.resurrected.contains(event.getPlayer())){
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent event){
		if(Core.resurrected.contains(event.getPlayer())){
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerRegainHealth(EntityRegainHealthEvent event){
		if(event.getEntity() instanceof Player){
			Player p = (Player) event.getEntity();
			if (event.getRegainReason() == RegainReason.SATIATED){
				if(Core.resurrected.contains(p)){
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onFoodChange(FoodLevelChangeEvent event){
		if(event.getEntity() instanceof Player){
			Player p = (Player) event.getEntity();
			if(Core.resurrected.contains(p)){
				event.setCancelled(true);
			}
		}
	}
}
