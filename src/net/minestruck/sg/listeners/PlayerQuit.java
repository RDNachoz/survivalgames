package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.BountyUtil;
import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerQuit implements Listener{

	@EventHandler
	public void playerQuit(PlayerQuitEvent event){
		Player p = event.getPlayer();
		if(GameStateUtil.gs == GameState.STARTED || GameStateUtil.gs == GameState.DEATHMATCH){
			if(Core.playing.contains(p.getName())){
				Location loc = p.getLocation().clone();
				Inventory inv = p.getInventory();
				for (ItemStack item : inv.getContents()) {
					if (item != null) {
						loc.getWorld().dropItemNaturally(loc, item.clone());
					}
				}
				inv.clear();
				Core.playing.remove(p.getName());
				BountyUtil.removeBounty(p);
				Bukkit.getServer().broadcastMessage("�c�l>> The bounty on �e" + p.getName() + " �c�lhas dispersed into the air!");
			}else{
				Core.spectating.remove(p.getName());
			}
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			p.getActivePotionEffects().clear();
		}
	}
}
