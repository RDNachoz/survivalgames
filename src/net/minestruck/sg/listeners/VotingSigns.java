package net.minestruck.sg.listeners;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;
import net.minestruck.sg.util.VotingUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class VotingSigns implements Listener{

	@EventHandler
	public void onClick(PlayerInteractEvent event){
		Player p = event.getPlayer();
		Block b = event.getClickedBlock();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST || b.getType() == Material.WALL_SIGN){
				if(GameStateUtil.getState() == GameState.VOTING){
					if(!Core.voted.contains(p)){
						Sign s = (Sign) b.getState();
						if(s.getLine(0).equalsIgnoreCase("[Vote]")){
							VotingUtil.addVote(s.getLine(1));
							p.sendMessage("�a�l>> �eYou voted for �a" + s.getLine(1) + "�e!");
							Core.voted.add(p);
							for(Player all : Bukkit.getOnlinePlayers()){
								VotingUtil.createScoreboard(all);
							}
							s.setLine(3, String.valueOf(VotingUtil.getVotes(s.getLine(1))));
							s.update();
						}
					}else{
						p.sendMessage("�c�l>> YOU MAY ONLY VOTE ONCE!");
					}
				}else{
					p.sendMessage("�c�l>> YOU MAY NOT VOTE RIGHT NOW!");
				}
			}
		}
	}

	@EventHandler
	public void onFall(PlayerMoveEvent event){
		Player p = event.getPlayer();
		Location loc = p.getLocation();
		Location sloc = p.getWorld().getSpawnLocation();
		double y = loc.getY();
		if(GameStateUtil.getState() == GameState.VOTING){
			if(y <= 20){
				p.teleport(sloc);
			}
		}
	}
}
