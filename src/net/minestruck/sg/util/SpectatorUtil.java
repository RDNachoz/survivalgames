package net.minestruck.sg.util;

import java.util.ArrayList;

import net.minestruck.sg.Core;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SpectatorUtil{
	public static ItemStack tributes = new ItemStack(Material.ENCHANTED_BOOK);
	public static ItemStack mapCenter = new ItemStack(Material.WATCH);
	public static ItemStack hub = new ItemStack(Material.BED);
	public static ItemStack bone = new ItemStack(Material.BONE);

	@SuppressWarnings("deprecation")
	public static void spectate(Player p){
		Core.spectating.add(p.getName());
		p.setGameMode(GameMode.SURVIVAL);
		Location loc = p.getLocation();
		loc.add(0,5,0);
		p.teleport(loc);
		p.setAllowFlight(true);
		p.setFlying(true);
		for(Player all : Bukkit.getOnlinePlayers()){
			all.hidePlayer(p);
		}

		ItemMeta tmeta = tributes.getItemMeta();
		ArrayList<String> tlore = new ArrayList<String>();
		tlore.add("�3�lCLICK ME �7to view tributes.");
		tmeta.setDisplayName("�5�lTRIBUTES");
		tmeta.setLore(tlore);
		tributes.setItemMeta(tmeta);
		p.getInventory().setItem(0, tributes);

		ItemMeta cmeta = mapCenter.getItemMeta();
		ArrayList<String> clore = new ArrayList<String>();
		clore.add("�3�lCLICK ME �7to go to the map center.");
		cmeta.setDisplayName("�a�lTELEPORT TO MAP CENTER");
		cmeta.setLore(clore);
		mapCenter.setItemMeta(cmeta);
		p.getInventory().setItem(7, mapCenter);

		ItemMeta hmeta = hub.getItemMeta();
		ArrayList<String> hlore = new ArrayList<String>();
		hlore.add("�3�lCLICK ME �7to go back to the hub.");
		hmeta.setDisplayName("�6�lGO BACK TO THE HUB");
		hmeta.setLore(hlore);
		hub.setItemMeta(hmeta);
		p.getInventory().setItem(8, hub);
		
		ItemMeta bmeta = bone.getItemMeta();
		ArrayList<String> blore = new ArrayList<String>();
		blore.add("�7�lCOMING SOON");
		bmeta.setDisplayName("�c�lResurrect");
		bmeta.setLore(blore);
		bone.setItemMeta(bmeta);
		p.getInventory().setItem(4, bone);
		p.updateInventory();
	}
}
