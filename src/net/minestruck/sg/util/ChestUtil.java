package net.minestruck.sg.util;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

public class ChestUtil {
	public static void fillChest(Chest c) {
		calcRandom(c, new ItemStack(Material.PORK, 5), 0.15);
		calcRandom(c, new ItemStack(Material.STONE_SWORD, 1), 0.085);
		calcRandom(c, new ItemStack(Material.WOOD_AXE, 1), 0.35);
		calcRandom(c, new ItemStack(Material.WOOD_SWORD, 1), 0.25);
		calcRandom(c, new ItemStack(Material.IRON_CHESTPLATE, 1), 0.025);
		calcRandom(c, new ItemStack(Material.IRON_LEGGINGS, 1), 0.035);
		calcRandom(c, new ItemStack(Material.IRON_HELMET, 1), 0.055);
		calcRandom(c, new ItemStack(Material.IRON_BOOTS, 1), 0.065);
		calcRandom(c, new ItemStack(Material.LEATHER_CHESTPLATE, 1), 0.095);
		calcRandom(c, new ItemStack(Material.LEATHER_LEGGINGS, 1), 0.10);
		calcRandom(c, new ItemStack(Material.LEATHER_HELMET, 1), 0.15);
		calcRandom(c, new ItemStack(Material.LEATHER_BOOTS, 1), 0.25);
		calcRandom(c, new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1), 0.085);
		calcRandom(c, new ItemStack(Material.CHAINMAIL_LEGGINGS, 1), 0.085);
		calcRandom(c, new ItemStack(Material.CHAINMAIL_HELMET, 1), 0.1);
		calcRandom(c, new ItemStack(Material.CHAINMAIL_BOOTS, 1), 0.1);
		calcRandom(c, new ItemStack(Material.IRON_INGOT, 1), 0.075);
		calcRandom(c, new ItemStack(Material.GOLD_INGOT, 2), 0.15);
		calcRandom(c, new ItemStack(Material.STICK, 1), 0.2);
		calcRandom(c, new ItemStack(Material.APPLE, 1), 0.25);
		calcRandom(c, new ItemStack(Material.RAW_BEEF, 3), 0.25);
		calcRandom(c, new ItemStack(Material.RAW_CHICKEN, 2), 0.35);
		calcRandom(c, new ItemStack(Material.RAW_FISH, 2), 0.25);
		calcRandom(c, new ItemStack(Material.ROTTEN_FLESH, 2), 0.25);
		calcRandom(c, new ItemStack(Material.COOKED_BEEF, 1), 0.15);
		calcRandom(c, new ItemStack(Material.COOKED_FISH, 1), 0.15);
		calcRandom(c, new ItemStack(Material.COOKED_MUTTON, 1), 0.15);
		calcRandom(c, new ItemStack(Material.COOKED_CHICKEN, 1), 0.15);
		calcRandom(c, new ItemStack(Material.COOKED_RABBIT, 1), 0.15);
		calcRandom(c, new ItemStack(Material.BAKED_POTATO, 1), 0.3);
		calcRandom(c, new ItemStack(Material.CARROT_ITEM, 1), 0.25);
		calcRandom(c, new ItemStack(Material.POTATO_ITEM, 1), 0.25);
		calcRandom(c, new ItemStack(Material.POISONOUS_POTATO, 1), 0.15);
		calcRandom(c, new ItemStack(Material.DIAMOND, 1), 0.015);
		calcRandom(c, new ItemStack(Material.WEB, 1), 0.075);
		calcRandom(c, new ItemStack(Material.SNOW_BALL, 1), 0.085);
		calcRandom(c, new ItemStack(Material.EGG, 1), 0.085);
		calcRandom(c, new ItemStack(Material.FISHING_ROD, 1), 0.15);

	}



	private static boolean calcRandom(Chest c, ItemStack item, double probability) {

		if ((new Random()).nextInt(100) < probability * 100) {
			int size = c.getInventory().getSize();
			int slot = new Random().nextInt(size);
			//c.getInventory().clear();
			c.getInventory().setItem(slot, item);
		}
		return true;
	}
}
