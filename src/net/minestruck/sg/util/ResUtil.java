package net.minestruck.sg.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import net.minestruck.sg.Core;
import net.minestruck.sg.util.GameStateUtil.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class ResUtil {
	public static boolean hasPass(Player p){
		String query = "SELECT `passes` FROM `passes` WHERE `uuid`= '"+p.getUniqueId().toString()+"' AND `passes` > 0;";
		try {
			ResultSet res = Core.connection.prepareStatement(query).executeQuery();
			if(res.next() == false){
				return false;
			}else{
				return true;
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}
	public static void spendPass(Player p){
		String query = "UPDATE `passes` SET `passes`=`passes` - 1 WHERE `uuid` = '"+p.getUniqueId().toString()+"'";
		try {
			PreparedStatement ps = Core.connection.prepareStatement(query);
			ps.execute();
			ps.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	public static void givePass(Player p){
		String query = "UPDATE `passes` SET `passes`=`passes` + 1 WHERE `uuid` = '"+p.getUniqueId().toString()+"'";
		try {
			PreparedStatement ps = Core.connection.prepareStatement(query);
			ps.execute();
			ps.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	public static void resurrect(final Player resurrecting, final Player target){
		if(!Core.playing.contains(target)){
			resurrecting.sendMessage("�c�lYOUR TARGET HAS DIED AND CAN NO LONGER BE TARGETTED!");
			return;
		}
		else if(Core.resurrections.containsKey(resurrecting) && Core.resurrections.containsValue(target)){
			if(GameStateUtil.getState() != GameState.DEATHMATCH || GameStateUtil.getState() != GameState.PREDM){
				if(!Core.resurrected.contains(resurrecting)){
					if(!Core.resurrected.contains(target)){
						if(hasPass(resurrecting)){
							new BukkitRunnable(){
								int time = 16;
								@Override
								public void run(){
									time--;
									if(time == 15){
										Bukkit.getServer().broadcastMessage("�e�l" + resurrecting.getName() + " �c�lSEEKS REVENGE ON �e�l" + target.getName() + "�c�l!");
									}
									else if(time < 14 && time > 0){
										Bukkit.getServer().broadcastMessage("�e�l" + resurrecting.getName() + " �c�lRESURRECTS IN �b" + time + " �c�lSECONDS!");
										for(Player all : Bukkit.getOnlinePlayers()){
											all.playSound(all.getLocation(), Sound.ENDERMAN_TELEPORT, 10, 10);
										}
									}
									else if(time == 0){
										Bukkit.getServer().broadcastMessage("�c�l" + resurrecting.getName() + " �c�lHAS RESURRECTED AND SEEKS REVENGE ON �e�l" + target.getName() + "�c�l!");
										if(Core.spectating.contains(resurrecting.getName())){
											double x = Core.centerx.get(0);
											double y = Core.centery.get(0);
											double z = Core.centerz.get(0);
											World w = Bukkit.getWorld(Core.currentMap.get(0));
											Location center = new Location(w, x, y, z);
											MobDisguise d = new MobDisguise(DisguiseType.WOLF);
											DisguiseAPI.disguiseToAll(resurrecting, d);
											resurrecting.teleport(center);
											resurrecting.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000000, 0));
											for(Player all : Bukkit.getOnlinePlayers()){
												all.showPlayer(resurrecting);
											}
											Core.spectating.remove(resurrecting.getName());
											resurrecting.setAllowFlight(false);
											resurrecting.setFlying(false);

											ItemStack sword = new ItemStack(Material.WOOD_SWORD, 1);
											final ItemStack compass = new ItemStack(Material.COMPASS, 1);

											final ItemMeta cmeta = compass.getItemMeta();
											final ArrayList<String> lore = new ArrayList<String>();
											lore.add("�a�nTarget: " + "�e�l" + target.getName());
											new BukkitRunnable(){
												@Override
												public void run(){
													double distance = Math.round(resurrecting.getLocation().distance(target.getLocation()));
													cmeta.setDisplayName("�a�nTarget: " + "�e�l" + target.getName() + "       �a�nDistance: " + "�e�l" + distance);
													compass.setItemMeta(cmeta);
												}
											}.runTaskTimer(Core.getInstance(), 0L, 5L);
											sword.addUnsafeEnchantment(Enchantment.DURABILITY, 100);

											resurrecting.getInventory().clear();
											resurrecting.getInventory().setItem(0, sword);
											resurrecting.getInventory().setItem(1, compass);
											resurrecting.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
											resurrecting.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
											resurrecting.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
											resurrecting.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
											Core.resurrected.add(resurrecting);
											Core.spectating.remove(resurrecting.getName());
											spendPass(resurrecting);
											new BukkitRunnable(){
												@Override
												public void run(){
													if(Core.resurrected.contains(resurrecting)){
														resurrecting.setCompassTarget(target.getLocation());
													}
												}
											}.runTaskTimerAsynchronously(Core.getInstance(), 0L, 5L);
										}
										cancel();
									}
								}
							}.runTaskTimer(Core.getInstance(), 0L, 20L);
						}else{
							resurrecting.sendMessage("�c�lYou must have a pass to resurrect!");
						}
					}else{
						resurrecting.sendMessage("�c�lYOU DIED TO SOMEONE WHO HAS RESURRECTED! NO!");
					}
				}else{
					resurrecting.sendMessage("�c�lYOU HAVE ALREADY RESURRECTED!");
				}
			}else{
				resurrecting.sendMessage("�c�lYOU CAN'T RESURRECT RIGHT NOW!");
			}
		}else{
			resurrecting.sendMessage("�c�lYOU HAD TO BE PLAYING TO RESURRECT!");
		}
	}
}
