package net.minestruck.sg.util;

public class GameStateUtil {
	public static GameState gs = GameState.RESTARTING;
	
	public static enum GameState {
		VSTARTED, STARTED, VOTING, DEATHMATCH, RESTARTING, OFFLINE, PREGAME, PREDM;
	}
	
	public static void setState(GameState g){
		gs = g;
	}
	public static GameState getState(){
		return gs;
	}
}
