package net.minestruck.sg.util;

import java.io.File;

import net.minestruck.sg.Core;

import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

public class WorldUtil {
	public static void loadWorld(String wName) {
		WorldCreator creator = new WorldCreator(wName);
		creator.generateStructures(false);
		creator.type(WorldType.FLAT);
		creator.seed(0);
		Core.getInstance().getServer().createWorld(creator);
	}
	public static boolean deleteDirectory(File path){
		if(path.exists())
		{
			File[] files = path.listFiles();
			for(int i = 0; i < files.length; i++)
			{
				if(files[i].isDirectory())
				{
					deleteDirectory(files[i]);
				}
				else
				{
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}
}