package net.minestruck.sg.util;

import java.io.File;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.GameStateUtil.GameState;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GameUtil {
	static Core plugin;
	public GameUtil(Core instance){
		plugin = instance;
	}
	public static void win(OfflinePlayer p){
		Bukkit.broadcastMessage("�a�l>> �e�l" + p.getName().toUpperCase() + " �a�lHAS WON THE HUNGER GAMES!");
		for(Player all : Bukkit.getOnlinePlayers()){
			all.playSound(all.getLocation(), Sound.ENDERMAN_SCREAM, 5, 5);
		}
		Bukkit.getScheduler().cancelAllTasks();
		new BukkitRunnable(){
			int time = 5;
			@Override
			public void run(){
				time--;
				if(time == 4){
					Bukkit.broadcastMessage("�5�l>> �eI best see all of you out of this server in 5 seconds or I'll...");
				}
				else if(time < 4 && time > 0){
					Bukkit.broadcastMessage("�5�l>> �eKicking you in... " + time + " �eseconds!");
				}
				else if(time <= 0){
					endGame();
					cancel();
				}
			}
		}.runTaskTimer(Core.getInstance(), 100L, 20L);
	}
	public static void endGame(){
		for(Player all : Bukkit.getOnlinePlayers()){
			/*ByteArrayOutputStream b = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(b);

			try {
				out.writeUTF("Connect");
				out.writeUTF("hub"); // Target Server
			} catch (IOException e) {
				// Can never happen
			}
			all.sendPluginMessage(Core.getInstance(), "BungeeCord", b.toByteArray());*/
			all.kickPlayer("Meh.");
		}
		final World wtu = Bukkit.getWorld(Core.currentMap.get(0));
		final File remove = new File(wtu.getWorldFolder().getParentFile(), wtu.getName());
		Bukkit.getServer().unloadWorld(Core.currentMap.get(0), true);
		System.out.println("World unloaded.");
		WorldUtil.deleteDirectory(remove);
		System.out.println("Directory deleted");
		GameStateUtil.setState(GameState.RESTARTING);
		Bukkit.shutdown();
	}
}
