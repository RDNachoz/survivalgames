package net.minestruck.sg.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;

import net.minestruck.sg.Core;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class VotingUtil {
	public static ArrayList<Integer> added = new ArrayList<Integer>();

	/**Adds a vote*/
	public static void addVote(String map){
		if (!Core.votes.containsKey(map)) return;
		Integer value = Core.votes.get(map);
		value++;
		Core.votes.put(map, value);

	}
	/**Removes a vote*/
	public static void removeVote(String map){
		if (!Core.votes.containsKey(map)) return;
		Integer value = Core.votes.get(map);
		value--;
		Core.votes.put(map, value);
	}
	/**Removes a vote*/
	public static int getVotes(String map){
		return Core.votes.get(map);
	}

	public static int getTotalVotes(){
		int total = 0;
		for (Entry<String,Integer> entry : Core.votes.entrySet()){
			total += entry.getValue();
		}
		return total;
	}
	/**Makes the scoreboard*/
	public static void createScoreboard(Player p){
		Scoreboard board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		Objective o = board.registerNewObjective("sw", "dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		o.setDisplayName("�d�lVOTES");

		for (int i = 0; i < 4; i++){
			Score maps = o.getScore("�b"+ Core.maps.get(i));
			maps.setScore(getVotes(Core.maps.get(i)));
		}
		p.setScoreboard(board);
	}
	/**Selects the winning map*/
	public static void selectWinningMap(){
		//I probably did this wrong....
		Entry<String,Integer> winningMap = null;
		for (Entry<String, Integer> entry : Core.votes.entrySet()){
			if (winningMap == null || entry.getValue() < winningMap.getValue()) {
				winningMap = entry;
				//String worldName = winningMap.getKey();
				Core.currentMap.add("LostIslands");
			}
		}
	}

	public static void addMapsToList(){
		Core.maps.add("LostIslands");
		Core.maps.add("Venzezia");
		Core.maps.add("Dreamland");
		Core.maps.add("Carnival");
		Core.maps.add("Chilled");
		Core.maps.add("Avarcia");
		Core.maps.add("Drift");

		Collections.shuffle(Core.maps);

		for (int i = 0; i < 4; i++) {
			Core.votes.put(Core.maps.get(i), 0);
		}
	}
	public static void doSigns(World w) throws Exception{
		int x = 0;
		int z = 0;

		ArrayList<Block> signs = new ArrayList<Block>();
		for(x = w.getSpawnLocation().getChunk().getX() - 10; x <= w.getSpawnLocation().getChunk().getX() + 10; x++){
			for(z = w.getSpawnLocation().getChunk().getZ() - 10; z <= w.getSpawnLocation().getChunk().getZ() + 10; z++){

				Chunk chunk = w.getChunkAt(x, z);
				for(BlockState bs : chunk.getTileEntities()){
					if(bs instanceof Sign){
						Sign sign = (Sign) bs;
						if(sign.getLine(0).equalsIgnoreCase("[vote]")){
							signs.add(bs.getBlock());
						}
					}
				}
			}
		}
		if (signs.size() != 4){
			for (Block b : signs){
				((Sign) b.getState()).setLine(1, "Error");
			}
			throw new Exception("Too many/few [vote] signs");
		}
		for (int i = 0;i < 4;i++){
			Sign sign = (Sign) signs.get(i).getState();
			sign.setLine(1, Core.maps.get(i));
			sign.setLine(2, "�9�lCLICK TO VOTE");
			sign.setLine(3, "�d" + getVotes(Core.maps.get(i)));
			sign.update();
		}
	}
	public static void resetSigns(World w) throws Exception{
		int x = 0;
		int z = 0;

		ArrayList<Block> signs = new ArrayList<Block>();
		for(x = w.getSpawnLocation().getChunk().getX() - 10; x <= w.getSpawnLocation().getChunk().getX() + 10; x++){
			for(z = w.getSpawnLocation().getChunk().getZ() - 10; z <= w.getSpawnLocation().getChunk().getZ() + 10; z++){

				Chunk chunk = w.getChunkAt(x, z);
				for(BlockState bs : chunk.getTileEntities()){
					if(bs instanceof Sign){
						Sign sign = (Sign) bs;
						if(sign.getLine(0).equalsIgnoreCase("[vote]")){
							signs.add(bs.getBlock());
						}
					}
				}
			}
		}
		if (signs.size() != 4){
			for (Block b : signs){
				((Sign) b.getState()).setLine(1, "Error");
			}
			throw new Exception("Too many/few [vote] signs");
		}
		for (int i = 0;i < 4;i++){
			Sign sign = (Sign) signs.get(i).getState();
			sign.setLine(0, "[Vote]");
			sign.update();
		}
	}
}