package net.minestruck.sg.util;

import net.minestruck.sg.Core;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

public class SpawnUtil {
	Core plugin;
	public SpawnUtil(Core instance){
		plugin = instance;
	}
	public static void setupSpawns(World w){
		double x = 0;
		double y = 0;
		double z = 0;
		for(x = Core.centerx.get(0) - 50; x <= Core.centerx.get(0) + 50; x++){

			for(y = Core.centery.get(0) - 5; y <= Core.centery.get(0) + 5; y++){

				for(z = Core.centerz.get(0) - 50; z <= Core.centerz.get(0) + 50; z++){

					Chunk chunk = w.getChunkAt(new Location(w, x, y, z));
					for(BlockState bs : chunk.getTileEntities()){
						if(bs instanceof Sign){
							Sign sign = (Sign) bs;
							Location loc = sign.getLocation();
							if(sign.getLine(0).equalsIgnoreCase("[spawn]")){
								addSpawn(loc.add(0.5,0,0.5));
								loc.getBlock().breakNaturally();
								clearDrops(w);
							}
						}
					}

				}

			}

		}
	}
	public static void setupCenter(World w){
		for(Chunk c : w.getLoadedChunks()){
			for(BlockState bs : c.getTileEntities()){
				if(bs instanceof Sign){
					Sign sign = (Sign) bs;
					Location loc = sign.getLocation();
					if(sign.getLine(0).equalsIgnoreCase("[center]")){
						double x = sign.getX();
						double z = sign.getZ();
						double y = sign.getY();
						setCenter(x, y, z);
						loc.getBlock().breakNaturally();
						clearDrops(w);
					}
				}
			}
		}
	}
	public static void addSpawn(Location loc){
		Core.pSpawns.add(loc);
	}
	public static void setCenter(Double x, Double y, Double z){
		Core.centerx.add(x);
		Core.centerz.add(z);
		Core.centery.add(y);
	}
	public static void clearDrops(World world) {
		for(Entity ent : world.getEntities()) {
			if(!(ent instanceof Item)) continue;
			ent.remove();
		}
	}
}
