package net.minestruck.sg.util;

import net.minestruck.sg.Core;
import net.minestruck.sg.util.GameStateUtil.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import de.craftstuebchen.api.worldborder.WorldBorderAPI;

public class dmUtil {
	static Core plugin;
	public dmUtil(Core instance){
		plugin = instance;
	}

	public static void startDeathMatch(){
		//TODO teleport back to spawn locations
		Bukkit.getScheduler().cancelAllTasks();
		new BukkitRunnable(){
			int time = 300;
			@Override
			public void run(){
				time--;
				if(time == 299){
					Core.tpAll();
					for(Player all : Core.playing){
						all.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 6000, 1));
						all.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 5, 1));
						//Location loc = Bukkit.getPlayer("RDNachoz").getLocation();
						dmLockPlayers();
						doBorder();
						GameStateUtil.setState(GameState.PREDM);
					}
				}
				else if(time == 295){
					Bukkit.broadcastMessage("�c�l>> �eYou will be released in �c�l5 SECONDS");
				}
				else if(time == 294){
					Bukkit.broadcastMessage("�c�l>> �eYou will be released in �c�l4 SECONDS");
				}
				else if(time == 293){
					Bukkit.broadcastMessage("�c�l>> �eYou will be released in �c�l3 SECONDS");
				}
				else if(time == 292){
					Bukkit.broadcastMessage("�c�l>> �eYou will be released in �c�l2 SECONDS");
				}
				else if(time == 291){
					Bukkit.broadcastMessage("�c�l>> �eYou will be released in �c�l1 SECOND");
				}
				else if(time == 290){
					GameStateUtil.setState(GameState.DEATHMATCH);
					Bukkit.broadcastMessage("�c�l>> �eFIGHT TO THE DEATH!");
					dmUnlockPlayers();
					double x = 0;
					double y = 0;
					double z = 0;
					World w = Bukkit.getWorld(Core.currentMap.get(0));
					for(x = Core.centerx.get(0) - 50; x <= Core.centerx.get(0) + 50; x++){

						for(y = Core.centery.get(0) - 5; y <= Core.centery.get(0) + 5; y++){

							for(z = Core.centerz.get(0) - 50; z <= Core.centerz.get(0) + 50; z++){

								Chunk chunk = w.getChunkAt(new Location(w, x, y, z));
								for(BlockState bs : chunk.getTileEntities()){
									if(bs instanceof Chest){
										Chest c = (Chest) bs;
										c.getInventory().clear();
										ChestUtil.fillChest(c);
										Core.filed.remove((Integer)0);
										Core.filed.add(1);
									}
								}
							}
						}
					}
					Bukkit.broadcastMessage("�c�l>> �a�lCHESTS HAVE BEEN RESTOCKED!");
				}
				else if(time == 270){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 4m:30s");
				}
				else if(time == 240){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 4m");
				}
				else if(time == 210){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 3m:30s");
				}
				else if(time == 180){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 3m");
				}
				else if(time == 150){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 2m:30s");
				}
				else if(time == 120){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 2m");
				}
				else if(time == 90){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN 1m:30s");
				}
				else if(time == 60){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN " + time);
				}
				else if(time == 30){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN " + time);
				}
				else if(time == 15){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN " + time);
				}
				else if(time < 10 && time > 0){
					Bukkit.broadcastMessage("�c�l>> DEATHMATCH ENDS IN " + time);
				}
			}
		}.runTaskTimer(Core.getInstance(), 0L, 20L);
	}

	public static void dmLockPlayers(){
		for(Player all : Bukkit.getOnlinePlayers()){
			all.setWalkSpeed(0.0F);
			all.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 10000, 128));
			all.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 6000, 3));
		}
	}
	public static void dmUnlockPlayers(){
		for(Player all : Bukkit.getOnlinePlayers()){
			all.setWalkSpeed(0.2F);
			all.removePotionEffect(PotionEffectType.JUMP);
			all.removePotionEffect(PotionEffectType.BLINDNESS);
		}
	}
	public static void doBorder(){
		/*double x = Core.centerx.get(0);
		double y = Core.centery.get(0);
		double z = Core.centerz.get(0);
		World w = Bukkit.getWorld(Core.currentMap.get(0));
		for(x = Core.centerx.get(0) - 50; x == Core.centerx.get(0) + 50; x++){

			for(y = Core.centery.get(0) - 10; y == Core.centery.get(0) + 10; y++){

				for(z = Core.centerz.get(0) - 50; z == Core.centerz.get(0) + 50; z++){
					Block b = w.getBlockAt(new Location(w, x, y, z));
					b.setType(Material.STAINED_GLASS);
				}
			}
		}*/
		for(Player all : Bukkit.getOnlinePlayers()){
			double x = Core.centerx.get(0);
			double y = Core.centery.get(0);
			double z = Core.centerz.get(0);
			World w = Bukkit.getWorld(Core.currentMap.get(0));
			Location loc = new Location(w, x, y, z);
			WorldBorderAPI.inst().setBorder(all, 100, loc);
		}
	}

}
