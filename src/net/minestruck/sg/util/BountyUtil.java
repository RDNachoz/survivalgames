package net.minestruck.sg.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;

import net.minestruck.sg.Core;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BountyUtil {
	public static HashMap<Player, Integer> bounties = new HashMap<Player, Integer>();

	public static void addPoints(Player p, int amount){
		String query = "UPDATE `passes` SET `points`=`points` + "+amount+" WHERE `uuid` = '"+p.getUniqueId().toString()+"'";
		try {
			PreparedStatement ps = Core.connection.prepareStatement(query);
			ps.execute();
			ps.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	public static void addBounty(Player p, int amount){
		if(bounties.containsKey(p)){
			bounties.values().add(amount);
			Bukkit.getServer().broadcastMessage("�d�l>> �a" + p.getName() + "�a's �d�lbounty was raised by �e" + amount + "�d�l!");
		}else{
			bounties.put(p, amount);
			Bukkit.getServer().broadcastMessage("�d�l>> A bounty of �e" + amount + " was applied to �a" + p.getName());
		}
	}
	public static int getPoints(Player p){
		String query = "SELECT `points` FROM `passes` WHERE `uuid`= '"+p.getUniqueId().toString()+"'";
		try {
			ResultSet res = Core.connection.prepareStatement(query).executeQuery();
			int temp = res.getType();
			return temp;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return 0;
		}
	}
	public static int getBounty(Player p){
		int bounty = 0;
		for(Entry<Player, Integer> entry : bounties.entrySet()){
			Player pl = entry.getKey();
			if(pl == p){
				bounty = entry.getValue();
			}

		}
		return bounty;
	}
	public static void removeBounty(Player p){
		bounties.remove(p);
	}
	public static void removePoints(Player p, int amount){
		String query = "UPDATE `passes` SET `points`= `points` - "+amount+"  WHERE `uuid`= '"+p.getUniqueId().toString()+"'";
		try {
			PreparedStatement ps = Core.connection.prepareStatement(query);
			ps.execute();
			ps.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	public static void giveBounty(Player p){
		for(Entry<Player, Integer> entry : bounties.entrySet()){
			Player pl = entry.getKey();
			if(pl == p){
				int bounty = entry.getValue();
				String query = "UPDATE `passes` SET `points`= `points` + "+bounty+"  WHERE `uuid`= '"+p.getUniqueId().toString()+"'";
				try {
					PreparedStatement ps = Core.connection.prepareStatement(query);
					ps.execute();
					ps.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
				Bukkit.getServer().broadcastMessage("�d�l> �a" + p.getName() + " �d�lhas claimed the �e" + bounty + " �d�lon �c" + pl.getName() + "�c's �d�lhead!");
			}
		}
	}
	public static boolean containsElement(String uuid){
		String query = "SELECT `uuid` FROM `passes` WHERE `uuid` = '"+ uuid +"'";
		try {
			ResultSet res = Core.connection.prepareStatement(query).executeQuery();
			boolean temp = res.next();
			return temp;
		} catch (SQLException ex) {
			ex.printStackTrace();
			return true;
		}
	}
	
	public static void addPlayer(Player p){
		String query = "INSERT INTO `passes`(`name`, `uuid`) VALUES ('"+p.getName()+"', '"+p.getUniqueId().toString()+"');";
		try {
			PreparedStatement ps = Core.connection.prepareStatement(query);
			ps.execute();
			ps.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
