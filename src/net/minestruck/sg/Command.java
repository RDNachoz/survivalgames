package net.minestruck.sg;

import net.minestruck.sg.util.BountyUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command implements CommandExecutor{

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("bounty")){
			String target = String.valueOf(args[0]);
			Integer amount = Integer.valueOf(args[1]);
			if(args.length == 2){
				if(Bukkit.getPlayer(target) != null){
					if(Core.playing.contains(Bukkit.getPlayer(target))){
						BountyUtil.addBounty(Bukkit.getPlayer(target), amount);
						BountyUtil.removePoints(p, amount);
					}else{
						p.sendMessage("�c�l>> THE PLAYER MUST BE IN GAME!");
					}
				}else{
					p.sendMessage("�c�l>> THAT PLAYER IS NOT ONLINE!");
				}
			}else{
				p.sendMessage("�c�l>> WRONG ARGUMENT COUNT... �eUsage: �a/bounty <player> <amount>");
			}
			return true;
		}
		return false;
	}


}
