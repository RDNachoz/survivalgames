package net.minestruck.sg;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import net.minestruck.sg.listeners.BlockEvent;
import net.minestruck.sg.listeners.ChatEvent;
import net.minestruck.sg.listeners.MOTDEvent;
import net.minestruck.sg.listeners.PlayerJoin;
import net.minestruck.sg.listeners.PlayerKillEvent;
import net.minestruck.sg.listeners.PlayerOpenChest;
import net.minestruck.sg.listeners.PlayerQuit;
import net.minestruck.sg.listeners.PlayerRespawn;
import net.minestruck.sg.listeners.ShopEvents;
import net.minestruck.sg.listeners.SpectatorUtils;
import net.minestruck.sg.listeners.VotingSigns;
import net.minestruck.sg.util.GameStateUtil;
import net.minestruck.sg.util.GameStateUtil.GameState;
import net.minestruck.sg.util.SpawnUtil;
import net.minestruck.sg.util.VotingUtil;
import net.minestruck.sg.util.WorldUtil;
import net.minestruck.sg.util.ZipUtil;
import net.minestruck.sg.util.dmUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class Core extends JavaPlugin implements CommandExecutor{

	public static ArrayList<Player> playing = new ArrayList<Player>();
	public static ArrayList<Player> resurrected = new ArrayList<Player>();
	public static ArrayList<String> spectating = new ArrayList<String>();
	public static ArrayList<Location> opened = new ArrayList<Location>();
	public static ArrayList<Location> pSpawns = new ArrayList<Location>();
	public static ArrayList<String> maps = new ArrayList<String>();
	public static HashMap<String, Integer> votes = new HashMap<String, Integer>();
	public static HashMap<Player, Player> resurrections = new HashMap<Player, Player>();
	public static ArrayList<Player> voted = new ArrayList<Player>();
	public static ArrayList<String> currentMap = new ArrayList<String>();
	public static ArrayList<Double> centerx = new ArrayList<Double>();
	public static ArrayList<Double> centerz = new ArrayList<Double>();
	public static ArrayList<Double> centery = new ArrayList<Double>();
	public static ArrayList<Integer> filed = new ArrayList<Integer>();


	public static Connection connection;

	private static Core instance = null;

	@Override
	public void onEnable(){
		
		saveDefaultConfig();
		
		instance = this;

		this.getCommand("bounty").setExecutor(new Command());

		getServer().getPluginManager().registerEvents(new PlayerJoin(this), this);
		getServer().getPluginManager().registerEvents(new BlockEvent(), this);
		getServer().getPluginManager().registerEvents(new MOTDEvent(), this);
		getServer().getPluginManager().registerEvents(new PlayerRespawn(), this);
		getServer().getPluginManager().registerEvents(new PlayerKillEvent(this), this);
		getServer().getPluginManager().registerEvents(new PlayerOpenChest(), this);
		getServer().getPluginManager().registerEvents(new SpectatorUtils(), this);
		getServer().getPluginManager().registerEvents(new PlayerQuit(), this);
		getServer().getPluginManager().registerEvents(new ShopEvents(), this);
		getServer().getPluginManager().registerEvents(new VotingSigns(), this);
		getServer().getPluginManager().registerEvents(new ChatEvent(), this);

		filed.add(0);
		VotingUtil.addMapsToList();

		GameStateUtil.setState(GameState.VOTING);
		try {
			VotingUtil.doSigns(Bukkit.getWorld("spawn"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		new BukkitRunnable(){
			@Override
			public void run(){
				openConnection();
			}
		}.runTaskLater(this, 20);
		gameTime();
	}

	@Override
	public void onDisable(){
		try {
			VotingUtil.resetSigns(Bukkit.getWorld("spawn"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		opened.clear();
		maps.clear();
		spectating.clear();
		playing.clear();
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "worldborder set 60000000");
		final World wtu = Bukkit.getWorld(currentMap.get(0));
		final File remove = new File(wtu.getWorldFolder().getParentFile(), wtu.getName());
		Bukkit.getServer().unloadWorld(wtu, true);
		System.out.println("World unloaded.");
		WorldUtil.deleteDirectory(remove);
		System.out.println("Directory deleted");
		GameStateUtil.setState(GameState.RESTARTING);
		currentMap.clear();
	}

	public synchronized static void openConnection(){
		String ip = (String) Core.getInstance().getConfig().get("database.ip");
		String port = (String) Core.getInstance().getConfig().get("database.port");
		String db = (String) Core.getInstance().getConfig().get("database.database");
		String user = (String) Core.getInstance().getConfig().get("database.user");
		String password = (String) Core.getInstance().getConfig().get("database.password");
		try {
			connection = DriverManager.getConnection("jdbc:mysql://" + ip +":" + port + "/" + db, user, password);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	public static Core getInstance(){
		return instance;
	}
	public void gameTime(){
		new BukkitRunnable(){
			int time = 1650;
			@Override
			public void run(){
				time--;
				if(time== 1650){
					Bukkit.getServer().broadcastMessage("�b�l>> �e2 minutes left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1620){
					Bukkit.getServer().broadcastMessage("�b�l>> �e90 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1575){
					Bukkit.getServer().broadcastMessage("�b�l>> �e45 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1560){
					Bukkit.getServer().broadcastMessage("�b�l>> �e30 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1545){
					Bukkit.getServer().broadcastMessage("�b�l>> �e15 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1540){
					Bukkit.getServer().broadcastMessage("�b�l>> �e10 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1535){
					Bukkit.getServer().broadcastMessage("�b�l>> �e5 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1534){
					Bukkit.getServer().broadcastMessage("�b�l>> �e4 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1533){
					Bukkit.getServer().broadcastMessage("�b�l>> �e3 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1532){
					Bukkit.getServer().broadcastMessage("�b�l>> �e2 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1531){
					Bukkit.getServer().broadcastMessage("�b�l>> �e1 seconds left to vote!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if(time == 1530){
					if(Bukkit.getOnlinePlayers().size() >= 1){
						VotingUtil.selectWinningMap();
						Bukkit.getServer().broadcastMessage("�e�l>> �aThe map �e" + currentMap.get(0) + " �ahas won!");
						Bukkit.getServer().broadcastMessage("�a�l>> �7�oLoading map...");
						String wName = currentMap.get(0);
						File f = new File("");
						f= new File(f.getAbsolutePath());
						File worldZip = new File(wName + ".zip");
						try {
							ZipUtil.extractZIP(worldZip, f);
						} catch (IOException e) {
							e.printStackTrace();
						}
						WorldUtil.loadWorld(wName);
						Bukkit.getServer().broadcastMessage("�a�l>> �7�oMap Loaded.");
						SpawnUtil.setupCenter(Bukkit.getWorld(wName));
						SpawnUtil.setupSpawns(Bukkit.getWorld(wName));
						Bukkit.getServer().broadcastMessage("�c�l>> �7�oTeleporting players...");
						GameStateUtil.setState(GameState.PREGAME);
						tpAll();
						World w = Bukkit.getWorld(wName);
						w.setGameRuleValue("doMobSpawning", "false");
						w.setGameRuleValue("doDaylightCycle", "false");
						for(Player all : Core.playing){
							ScoreboardManager manager = Bukkit.getScoreboardManager();
							Scoreboard board = manager.getNewScoreboard();
							all.setScoreboard(board);
							all.getActivePotionEffects().clear();
						}
					}else{
						Bukkit.getServer().broadcastMessage("�c�l>> �7�oNot enough players to start... resetting timer!");
						time = 1650;
					}
				}
				else if (time == 1500){
					Bukkit.getServer().broadcastMessage("�b�l>> �e30 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1485){
					Bukkit.getServer().broadcastMessage("�b�l>> �e15 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1480){
					Bukkit.getServer().broadcastMessage("�b�l>> �e10 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1475){
					Bukkit.getServer().broadcastMessage("�b�l>> �e5 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1474){
					Bukkit.getServer().broadcastMessage("�b�l>> �e4 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1473){
					Bukkit.getServer().broadcastMessage("�b�l>> �e3 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1472){
					Bukkit.getServer().broadcastMessage("�b�l>> �e2 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1471){
					Bukkit.getServer().broadcastMessage("�b�l>> �e1 seconds left until the game starts!");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.CHICKEN_EGG_POP, 5, 5);
					}
				}
				else if (time == 1470){
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getLocation(),Sound.EXPLODE, 10, 5);
						GameStateUtil.setState(GameState.STARTED);
						all.getInventory().addItem(new ItemStack(Material.COMPASS, 1));
					}
					Bukkit.getServer().broadcastMessage("�b�l>> �eGO! GO! GO!");
				}
				else if (time == 1440){
					Bukkit.getServer().broadcastMessage("�b�l>> �e24 minutes left until game deathmatch starts!");
				}
				else if (time == 1200){
					Bukkit.getServer().broadcastMessage("�b�l>> �e20 minutes left until game deathmatch starts!");
				}
				else if (time == 900){
					Bukkit.getServer().broadcastMessage("�b�l>> �e15 minutes left until game deathmatch starts!");
				}
				else if (time == 600){
					Bukkit.getServer().broadcastMessage("�b�l>> �e10 minutes left until game deathmatch starts!");
				}
				else if (time == 540){
					Bukkit.getServer().broadcastMessage("�b�l>> �e9 minutes left until game deathmatch starts!");
				}
				else if (time == 480){
					Bukkit.getServer().broadcastMessage("�b�l>> �e8 minutes left until game deathmatch starts!");
				}
				else if (time == 420){
					Bukkit.getServer().broadcastMessage("�b�l>> �e7 minutes left until game deathmatch starts!");
				}
				else if (time == 360){
					Bukkit.getServer().broadcastMessage("�b�l>> �e6 minutes left until game deathmatch starts!");
				}
				else if (time == 300){
					Bukkit.getServer().broadcastMessage("�b�l>> �e5 minutes left until game deathmatch starts!");
				}
				else if (time == 240){
					Bukkit.getServer().broadcastMessage("�b�l>> �e4 minutes left until game deathmatch starts!");
				}
				else if (time == 180){
					Bukkit.getServer().broadcastMessage("�b�l>> �e3 minutes left until game deathmatch starts!");
				}
				else if (time == 120){
					Bukkit.getServer().broadcastMessage("�b�l>> �e2 minutes left until game deathmatch starts!");
				}
				else if (time == 60){
					Bukkit.getServer().broadcastMessage("�b�l>> �e1 minute left until game deathmatch starts!");
				}
				else if(time == 30){
					Bukkit.getServer().broadcastMessage("�b�l>> �e"+ time +"�eseconds left until game deathmatch starts!");
				}
				else if(time == 15){
					Bukkit.getServer().broadcastMessage("�b�l>> �e"+ time +"�eseconds left until game deathmatch starts!");
				}
				else if(time == 10){
					Bukkit.getServer().broadcastMessage("�b�l>> �e"+ time +"�eseconds left until game deathmatch starts!");
				}
				else if(time < 5 && time > 0){
					Bukkit.getServer().broadcastMessage("�b�l>> �e"+ time +"�eseconds left until game deathmatch starts!");
				}
				else if(time == 0){
					GameStateUtil.setState(GameState.DEATHMATCH);
					dmUtil.startDeathMatch();
					cancel();
				}
			}
		}.runTaskTimer(instance, 0L, 20L);
	}
	public static void tpAll(){
		int i = 0;
		for(Player all : Core.playing){
			all.teleport(pSpawns.get(i));
			i++;
		}
	}
	public static Player getNearest(Player p, Double range) {
		double distance = Double.POSITIVE_INFINITY;
		Player target = null;
		for (Entity e : p.getNearbyEntities(range, range, range)) {
			if (!(e instanceof Player))
				continue;
			if(e == p) continue;
			double distanceto = p.getLocation().distance(e.getLocation());
			if (distanceto > distance)
				continue;
			distance = distanceto;
			target = (Player) e;
		}
		return target;
	}
}